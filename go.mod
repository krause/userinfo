module git.mpib-berlin.mpg.de/krause/userinfo

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.0
	golang.org/x/text v0.3.6
)
