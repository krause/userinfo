package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/cookiejar"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"encoding/json"
	"io/ioutil"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/text/cases"
)

// Userinfo is a userinfo object
type Userinfo struct {
	username string
	fullname string
	phone    string
	room     string

}

type MaxObject struct {
	FilterParams string `json:"filter_params"`
	Employees []MaxUser `json:"employees"`
	Total int `json:"total"`
}

type MaxUser struct {
	Username string `json:"web_id"`
	Firstname string `json:"first_name"`
	Lastname string `json:"last_name"`
	Phones []string `json:"phone_numbers"`
	Room string `json:"room"`
}


const (
	baseURL = "https://intra.mpib-berlin.mpg.de/de/i/personen"
	pageURL = baseURL + "?vdt=intra_mpi_personen_a_bis_z|page_"
	debug   = false
	version = "1.0.0"
)

var c = cases.Fold()

func userinfoFmt(userinfo Userinfo, delimiter bool) string {
	s := ""
	if delimiter {
		s = "--------------------------------\n"
	}
	return fmt.Sprintf("%sName: %s\nRoom: %s, Phone: %s", s, userinfo.fullname, userinfo.room, userinfo.phone)
}

// only helpful to debug multiple first request redirects
func redirectPolicyFunc(req *http.Request, via []*http.Request) error {
	if req.Response.Request.Response != nil {
		if debug {
			log.Printf("Redirecting to %s from %s [%d]\n", req.URL,
				req.Response.Request.URL, req.Response.StatusCode)
			log.Println(req.Response.Cookies())
		}
	}
	return nil
}

func intraFetch(client *http.Client, pageIndex int, c chan Userinfo) error {
	if debug {
		log.Printf("fetching page %d\n", pageIndex)
	}
	resp, err := client.Get(fmt.Sprintf("%s%d", pageURL, pageIndex))
	if err != nil {
		if debug {
			log.Printf("fetching page %d failed with: %s\n", pageIndex, err)
		}
		close(c)
		return err
	}
	defer resp.Body.Close()
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	doc.Find(".phone-table tr").Not(".sep-row").Not(".empty-row").Each(func(i int, s *goquery.Selection) {
		if debug {
			log.Println(s.Html())
		}
		result := Userinfo{}
		result.username = s.Find("td span span").First().Text()
		result.username = strings.Replace(result.username, " [dot] ", ".", -1)
		result.fullname = s.Find("td a").First().Text()
		result.room = s.Find("td span.raum").First().Text()
		result.phone = s.Find("td span.telefon").First().Text()
		if i := strings.Index(result.phone, "-"); i != -1 {
			result.phone = result.phone[i+1:]
		}
		c <- result
	})
	close(c)
	return nil
}

func maxProvider(results map[string]Userinfo) error {
	client := &http.Client{
		CheckRedirect: redirectPolicyFunc, // debug helper
		Timeout:       time.Second * 10,    // default is unlimited X.X
	}

	req, err := http.NewRequest("GET", "https://employees.ixedit.mpg.de/rest/v1/employees", nil)
	if err != nil {
		return errors.New("error creating http request")
	}
	req.SetBasicAuth("mpib", "ec5f7c041b61f558219b63f2c9a05d6c")
	resp, err := client.Do(req)
	if err != nil {
		return errors.New("fetching maxIntra failed, check authentication data")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.New("error reading bytes from response body")
	}


	max := MaxObject{}
	json.Unmarshal(body, &max)
	for _, employee := range max.Employees {
		result := Userinfo{}
		result.username = employee.Username
		result.fullname = fmt.Sprintf("%s %s",employee.Firstname, employee.Lastname)
		result.room = employee.Room
		var extension string
		if len(employee.Phones) > 0 {
			extension = strings.Split(employee.Phones[0], "-")[1]
		} else {
			extension = "not listed"
		}
		result.phone = extension
		results[result.username] = result
	}
	return nil
}

func intraProvider(results map[string]Userinfo) error {
	// override some http client defaults
	jar, _ := cookiejar.New(nil)
	client := &http.Client{
		CheckRedirect: redirectPolicyFunc, // debug helper
		Timeout:       time.Second * 3,    // default is unlimited X.X
		Jar:           jar,                // cookies aren't used if nil
	}

	// fill the cookie jar first
	resp, err := client.Get(baseURL)
	if err != nil {
		return errors.New("Fetching intranet failed, are you on the correct network?")
	}
	defer resp.Body.Close()

	// define the number of tabs to fetch with channel count
	var fetchChannels [7]chan Userinfo
	// concurrent fetch to individual channels
	for pageIndex := range fetchChannels {
		fetchChannels[pageIndex] = make(chan Userinfo)
		go intraFetch(client, pageIndex, fetchChannels[pageIndex])
	}
	// serial reduce
	for pageIndex := range fetchChannels {
		for result := range fetchChannels[pageIndex] {
			results[result.username] = result
		}
	}
	return nil
}

func lookup(results map[string]Userinfo, searchKey string) {
	if _, err := strconv.Atoi(searchKey); err == nil {
		if debug {
			log.Println("looking up by phone")
		}
		for _, userinfo := range results {
			if userinfo.phone == searchKey {
				fmt.Println(userinfoFmt(userinfo, false))
			}
		}
	} else {
		if debug {
			log.Println("looking up by name")
		}
		count := 0
		for _, userinfo := range results {
			i1 := strings.Index(c.String(userinfo.username), c.String(searchKey))
			i2 := strings.Index(c.String(userinfo.fullname), c.String(searchKey))
			if i1 != -1 || i2 != -1 {
				fmt.Println(userinfoFmt(userinfo, count > 0))
				count++
			}
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Printf("Usage: %s <search_key>\n\n", filepath.Base(os.Args[0]))
		fmt.Print("   please report bugs to: https://git.mpib-berlin.mpg.de/krause/userinfo\n\n")
	}
	flag.Parse()
	if len(flag.Args()) != 1 {
		flag.Usage()
		os.Exit(1)
	}

	// go fetch/populate results map
	results := make(map[string]Userinfo)
	if err := maxProvider(results); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// other providers go here..
	lookup(results, os.Args[1])
}
