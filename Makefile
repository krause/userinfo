build:
	GOOS=linux GOARCH=amd64 go build -o dist/linux/phone userinfo.go
	GOOS=windows GOARCH=amd64 go build -o dist/windows/phone.exe userinfo.go
	GOOS=darwin GOARCH=amd64 go build -o dist/darwin/phone userinfo.go

clean:
	rm -rf dist/

deploy:
	rsync -vr dist root@puppet.mpib-berlin.mpg.de:/etc/puppetlabs/files/blobs/mpib_phone/
