#!/usr/bin/env python3
# encoding: utf-8


"""Minimal python commad line."""

import sys
import argparse
import asyncio
import aiohttp
from bs4 import BeautifulSoup


__version__ = '1.0.0'


class SearchResult():

    def __init__(self, username, full_name, phone, room):
        self.username = username
        self.full_name = full_name
        self.phone = phone
        self.room = room

    def __str__(self):
        res = 'Name:  {}\n'.format(self.full_name)
        res += 'Phone: {:>3}, Room: {}'.format(self.phone, self.room)
        return res

    def __repr__(self):
        return '{}: <"{}", {}, {}>'.format(self.username, self.full_name,
                                           self.phone, self.room)


class SearchProvider():

    def __init__(self):
        self.results = {}
        asyncio.run(self.populate())

    async def populate(self):
        pass

    def search(self, search_key, by='name'):
        search_key = search_key.casefold()
        results = []
        for username, details in self.results.items():
            if by == 'name':
                if search_key in username.casefold() or \
                   search_key in details.full_name.casefold():
                    results.append({username: details})
            if by == 'phone':
                if search_key in details.phone.casefold():
                    results.append({username: details})
        return results


class IntraProvider(SearchProvider):
    base_url = 'https://intra.mpib-berlin.mpg.de/de/i/personen'
    page_url = base_url + '?vdt=intra_mpi_personen_a_bis_z%7Cpage_'

    async def get(self, session, page_index):
        resp = await session.get(self.page_url + str(page_index))
        data = await resp.text()
        return data

    async def populate(self):
        async with aiohttp.ClientSession() as session:
            # save cookies first (1000ms ...)
            await session.get(self.page_url)
            tasks = []
            for page_index in range(1, 8):  # unclear how many tabs there are
                tasks.append(self.get(session, page_index))
            htmls = await asyncio.gather(*tasks)
        for html in htmls:
            soup = BeautifulSoup(html, 'html.parser')
            entries = soup.find('table', {'class': 'phone-table'})\
                .find_all('tr', {'class': ''})
            for entry in entries:
                try:
                    full_name = entry.a.text
                    details = entry.find_all('td')
                    phone = ''
                    room = ''
                    if '-' in details[1].text:
                        phone = details[1].text.split('-')[-1]
                    if details[2].span is not None and \
                       details[2].span.span is not None:
                        username = details[2].span.span.text
                    if details[3].span is not None:
                        room = details[3].span.text
                    self.results[username] = SearchResult(username, full_name,
                                                          phone, room)
                except Exception as error:
                    print(error)


class ADProvider(SearchProvider):
    pass


def lookup(search_key, by='name'):
    results = {}
    for provider in [IntraProvider(), ADProvider()]:
        for result in provider.search(search_key, by=by):
            results.update(result)
    return [v for k, v in results.items()]


def display(results):
    if results is None or len(results) == 0:
        print('No match found')
    else:
        print(results[0])
        if len(results) > 1:
            for result in results[1:]:
                print('-' * 30)
                print(result)


def parse_command_line(argv):
    """Parse command line argument. See -h option

    :param argv: arguments on the command line must include caller file name.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", action="version",
                        version="%(prog)s {}".format(__version__))
    parser.add_argument('search_key', metavar='key',
                        help='lookup a name a name or a phone number')
    arguments = parser.parse_args(argv[1:])
    return arguments


def main():
    """Main program. Sets up logging and do some work."""
    try:
        args = parse_command_line(sys.argv)
        try:
            assert len(args.search_key) == 3
            _ = int(args.search_key)
            by = 'phone'
        except (AssertionError, ValueError):
            by = 'name'
        results = lookup(args.search_key, by=by)
        display(results)
    except KeyboardInterrupt:
        print('Program interrupted!')


if __name__ == "__main__":
    sys.exit(main())
