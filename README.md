Userinfo script
---------------

A simple tool to lookup basic userdata either by name or phone number.

```
$ go build -o phone userinfo.go
$ phone krause
Name: Krause, Michael
Room: 42, Phone: 471
```

Details
-------

This is useful, because our AD is not properly populated.  The code can be
extended to use multiple data sources. Up until 2021 this code parsed
https://intra.mpib-berin.mpg.de. Since then we have a new intranet (maxnet) and
an incomplete rest-like API that returns JSON data.

An initial implementation used Python3. Intranet data is split across
6 different tabs, each takes around 1000ms to load, so asyncio was necessary.

Using go with goroutines and cross-platform compilation seems like the obvious
choice. I can't think of a better example project for Go to be honest.

This script is deployed to all MPIB clients with puppet.
